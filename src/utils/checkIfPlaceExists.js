import axios from "axios";

const checkIfPlaceExists = async (
  keyword,
  city,
  state,
  engine,
  accessToken
) => {
  const requestOptions = {
    headers: { "access-token": accessToken },
  };

  let url;
  switch (engine) {
    case "GOOGLE_PLACES":
      url = process.env.REACT_APP_GOOGLE_PLACES_API;
      break;
    case "YELP":
      url = process.env.REACT_APP_YELP_API;
      break;
    case "FOURSQUARE":
      url = process.env.REACT_APP_FOURSQUARE_API;
      break;
    default:
      break;
  }

  try {
    const { data } = await axios.get(
      `${url}/checkIfPlaceExists?keyword=${keyword}&city=${city}&state=${state}`,
      requestOptions
    );

    console.log(data);
    return data;
  } catch (err) {
    return err.response.data;
  }
};

export default checkIfPlaceExists;
