import extenso from "extenso";

const writeNumericAddress = (address) => {
  const addressComponents = address.split(" ");

  let formattedAddress = "";

  for (let i = 0; i < addressComponents.length; i++) {
    console.log(addressComponents[i]);
    if (!isNaN(addressComponents[i])) {
      formattedAddress = formattedAddress + " " + extenso(addressComponents[i]);
    } else {
      formattedAddress = formattedAddress + " " + addressComponents[i];
    }
  }

  formattedAddress = formattedAddress.substr(1);
  return formattedAddress;
};

export default writeNumericAddress;
