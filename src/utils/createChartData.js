const createChartData = (googlePlacesResult, yelpResult, foursquareResult) => {
  const dataTable = [];
  const wazeResult = googlePlacesResult;
  let correctAmount = 0;
  let incorrectAmount = 0;
  let existingAmount = 0;
  let unexistingAmount = 0;

  for (let key in googlePlacesResult) {
    if (googlePlacesResult[key] === "Correto")
      correctAmount = correctAmount + 1;
    if (googlePlacesResult[key] === "Incorreto")
      incorrectAmount = incorrectAmount + 1;
    if (googlePlacesResult[key] === "Existente")
      existingAmount = existingAmount + 1;
    if (googlePlacesResult[key] === "Inexistente")
      unexistingAmount = unexistingAmount + 1;
  }
  for (let key in wazeResult) {
    if (wazeResult[key] === "Correto") correctAmount = correctAmount + 1;
    if (wazeResult[key] === "Incorreto") incorrectAmount = incorrectAmount + 1;
    if (wazeResult[key] === "Existente") existingAmount = existingAmount + 1;
    if (wazeResult[key] === "Inexistente")
      unexistingAmount = unexistingAmount + 1;
  }
  for (let key in yelpResult) {
    if (yelpResult[key] === "Correto") correctAmount = correctAmount + 1;
    if (yelpResult[key] === "Incorreto") incorrectAmount = incorrectAmount + 1;
    if (yelpResult[key] === "Existente") existingAmount = existingAmount + 1;
    if (yelpResult[key] === "Inexistente")
      unexistingAmount = unexistingAmount + 1;
  }
  for (let key in foursquareResult) {
    if (foursquareResult[key] === "Correto") correctAmount = correctAmount + 1;
    if (foursquareResult[key] === "Incorreto")
      incorrectAmount = incorrectAmount + 1;
    if (foursquareResult[key] === "Existente")
      existingAmount = existingAmount + 1;
    if (foursquareResult[key] === "Inexistente")
      unexistingAmount = unexistingAmount + 1;
  }

  dataTable.push(["Status", "Quantidade"]);
  dataTable.push(["Correto", correctAmount + existingAmount]);
  dataTable.push(["Inexistente", unexistingAmount]);
  dataTable.push(["Incorreto", incorrectAmount]);

  console.log(dataTable);

  return dataTable;
};

export default createChartData;
