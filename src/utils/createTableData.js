const createTableData = (
  googlePlacesResult,
  googlePlacesInfo,
  yelpResult,
  yelpInfo,
  foursquareResult,
  foursquareInfo
) => {
  console.log(googlePlacesInfo);

  const googleData = {
    engine: "GOOGLE_MAPS",
    notFound: googlePlacesInfo.error === "INVALID_KEYWORD",
    companyName: googlePlacesInfo.name ? googlePlacesInfo.name : "",
    nameStatus: googlePlacesResult.name ? googlePlacesResult.name : "",
    companyAddress: googlePlacesInfo.address ? googlePlacesInfo.address : "",
    addressStatus: googlePlacesResult.address ? googlePlacesResult.address : "",
    phone: googlePlacesInfo.phone ? googlePlacesInfo.phone : "",
    phoneStatus: googlePlacesResult.phone ? googlePlacesResult.phone : "",
    openingHours: googlePlacesInfo.opening_hours
      ? googlePlacesInfo.opening_hours
      : "",
    openingHoursStatus: googlePlacesResult.opening_hours
      ? googlePlacesResult.opening_hours
      : "",
    photo: googlePlacesInfo.photo ? googlePlacesInfo.photo : "",
    photoStatus: googlePlacesInfo.photo
      ? googlePlacesInfo.photo != "not found"
        ? "Existente"
        : "Inexistente"
      : "Inexistente",
    url: googlePlacesInfo.url,
  };
  const wazeData = {
    engine: "WAZE",
    notFound: googlePlacesInfo.error === "INVALID_KEYWORD",
    companyName: googlePlacesInfo.name ? googlePlacesInfo.name : "",
    nameStatus: googlePlacesResult.name ? googlePlacesResult.name : "",
    companyAddress: googlePlacesInfo.address ? googlePlacesInfo.address : "",
    addressStatus: googlePlacesResult.address ? googlePlacesResult.address : "",
    phone: googlePlacesInfo.phone ? googlePlacesInfo.phone : "",
    phoneStatus: googlePlacesResult.phone ? googlePlacesResult.phone : "",
    openingHours: googlePlacesInfo.opening_hours
      ? googlePlacesInfo.opening_hours
      : "",
    openingHoursStatus: googlePlacesResult.opening_hours
      ? googlePlacesResult.opening_hours
      : "",
    photo: googlePlacesInfo.photo ? googlePlacesInfo.photo : "",
    photoStatus: googlePlacesInfo.photo
      ? googlePlacesInfo.photo != "not found"
        ? "Existente"
        : "Inexistente"
      : "Inexistente",
    url: googlePlacesInfo.url,
  };
  const yelpData = {
    engine: "YELP",
    notFound: yelpInfo.error === "BUSINESS_NOT_FOUND",
    companyName: yelpInfo.name ? yelpInfo.name : "",
    nameStatus: yelpResult.name ? yelpResult.name : "",
    companyAddress: yelpInfo.address ? yelpInfo.address : "",
    addressStatus: yelpResult.address ? yelpResult.address : "",
    phone: yelpInfo.phone ? yelpInfo.phone : "",
    phoneStatus: yelpResult.phone ? yelpResult.phone : "",
    openingHours: yelpInfo.opening_hours ? yelpInfo.opening_hours : "",
    openingHoursStatus: yelpResult.opening_hours
      ? yelpResult.opening_hours
      : "",
    photo: yelpInfo.photo ? yelpInfo.photo : "",
    photoStatus: yelpInfo.photo
      ? yelpInfo.photo != "not found"
        ? "Existente"
        : "Inexistente"
      : "Inexistente",
    url: yelpInfo.url,
  };
  const foursquareData = {
    engine: "FOURSQUARE",
    notFound: foursquareInfo.error === "VENUE_NOT_FOUND",
    companyName: foursquareInfo.name ? foursquareInfo.name : "",
    nameStatus: foursquareResult.name ? foursquareResult.name : "",
    companyAddress: foursquareInfo.address ? foursquareInfo.address : "",
    addressStatus: foursquareResult.address ? foursquareResult.address : "",
    phone: foursquareInfo.phone ? foursquareInfo.phone : "",
    phoneStatus: foursquareResult.phone ? foursquareResult.phone : "",
    openingHours: foursquareInfo.opening_hours
      ? foursquareInfo.opening_hours
      : "",
    openingHoursStatus: foursquareResult.opening_hours
      ? foursquareResult.opening_hours
      : "",
    photo: foursquareInfo.photo ? foursquareInfo.photo : "",
    photoStatus: foursquareInfo.photo
      ? foursquareInfo.photo != "not found"
        ? "Existente"
        : "Inexistente"
      : "",
    url: foursquareInfo.url,
  };

  const appleMapsData = {
    engine: "APPLE_MAPS",
    notFound: foursquareInfo.error === "VENUE_NOT_FOUND",
    companyName: foursquareInfo.name ? foursquareInfo.name : "",
    nameStatus: foursquareResult.name ? foursquareResult.name : "",
    companyAddress: foursquareInfo.address ? foursquareInfo.address : "",
    addressStatus: foursquareResult.address ? foursquareResult.address : "",
    phone: foursquareInfo.phone ? foursquareInfo.phone : "",
    phoneStatus: foursquareResult.phone ? foursquareResult.phone : "",
    openingHours: foursquareInfo.opening_hours
      ? foursquareInfo.opening_hours
      : "",
    openingHoursStatus: foursquareResult.opening_hours
      ? foursquareResult.opening_hours
      : "",
    photo: foursquareInfo.photo ? foursquareInfo.photo : "",
    photoStatus: foursquareInfo.photo
      ? foursquareInfo.photo != "not found"
        ? "Existente"
        : "Inexistente"
      : "",
    url: foursquareInfo.url,
  };

  const tableData = [
    googleData,
    yelpData,
    foursquareData,
    wazeData,
    appleMapsData,
  ];

  console.log(tableData);

  return tableData;
};

export default createTableData;
