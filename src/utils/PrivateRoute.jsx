import React from "react";
import { Route, Redirect } from "react-router-dom";
import { didNotRefreshPage } from "../services/checkRefresh";

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      didNotRefreshPage() ? <Component {...props} /> : <Redirect to="/step/1" />
    }
  />
);
