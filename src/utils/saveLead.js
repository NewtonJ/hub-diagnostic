import axios from "axios";

const saveLead = async (
  name,
  email,
  whatsapp,
  companyName,
  zipCode,
  companyAddress,
  companyAddressNumber,
  companyCity,
  companyState,
  companyPhone,
  correctPercent,
  incorrectPercent,
  unexistingPercent,
  accessToken,
  shareable
) => {
  const requestOptions = {
    headers: { "access-token": accessToken },
  };

  let url = `${window.location.origin.toString()}/?companyName=${companyName}&companyAddress=${companyAddress}&companyAddressNumber=${companyAddressNumber}&city=${companyCity}&state=${companyState}&companyPhone=${companyPhone}&zipCode=${zipCode}&whatsapp=${whatsapp}&userName=${name}&userEmail=${email}&shareable=true`;

  url = url.replace(/ /g, "%20");

  try {
    const data = await axios.post(
      `${process.env.REACT_APP_LEAD_API}`,
      {
        name: name,
        email: email,
        whatsapp: whatsapp,
        company_name: companyName,
        zipcode: zipCode,
        company_address: companyAddress,
        company_address_number: companyAddressNumber,
        company_city: companyCity,
        company_state: companyState,
        company_phone: companyPhone,
        correct_percent: correctPercent,
        incorrect_percent: incorrectPercent,
        unexisting_percent: unexistingPercent,
        origin: "Internet",
        shareable: shareable,
        url: url,
      },
      requestOptions
    );

    return data;
  } catch (err) {
    console.log(err.data);
  }
};

export default saveLead;
