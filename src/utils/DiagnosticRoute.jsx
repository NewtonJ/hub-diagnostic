import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isThereDiagnosticData } from "../services/checkDiagnosticData";

export const DiagnosticRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isThereDiagnosticData() ? <Component {...props} /> : <Redirect to="/" />
    }
  />
);
