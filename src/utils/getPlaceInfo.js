import axios from "axios";

const getPlaceInfo = async (engine, accessCode, keyword, address) => {
  const requestOptions = {
    headers: { "access-token": accessCode },
  };

  let url;
  switch (engine) {
    case "GOOGLE_PLACES":
      url = process.env.REACT_APP_GOOGLE_PLACES_API;
      break;
    case "YELP":
      url = process.env.REACT_APP_YELP_API;
      break;
    case "FOURSQUARE":
      url = process.env.REACT_APP_FOURSQUARE_API;
      break;
    default:
      break;
  }

  try {
    const { data } = await axios.get(
      `${url}/findPlaceByAddress?keyword=${keyword}&address=${address}&format=true`,
      requestOptions
    );

    console.log("Google Place data");
    console.log(data);

    return data;
  } catch (err) {
    console.log(err.response);
    return err.response.data;
  }
};

export default getPlaceInfo;
