import axios from "axios";

const getStatus = async (
  accessToken,
  engine,
  companyName,
  street,
  number,
  city,
  state,
  phone,
  zipcode
) => {
  const requestOptions = {
    headers: { "access-token": accessToken },
  };
  let url;
  switch (engine) {
    case "GOOGLE_PLACES":
      url = process.env.REACT_APP_GOOGLE_PLACES_API;
      break;
    case "YELP":
      url = process.env.REACT_APP_YELP_API;
      break;
    case "FOURSQUARE":
      url = process.env.REACT_APP_FOURSQUARE_API;
      break;
    default:
      break;
  }
  const query = `companyName=${companyName}&phone=${phone}&street=${street}&number=${number}&city=${city}&state=${state}&zipcode=${zipcode}`;
  try {
    const { data } = await axios.get(`${url}/status?${query}`, requestOptions);

    return data;
  } catch (err) {
    if (
      err.response.data.error === "BUSINESS_NOT_FOUND" ||
      err.response.data.error === "VENUE_NOT_FOUND"
    ) {
      return {
        name: "Inexistente",
        address: "Inexistente",
        phone: "Inexistente",
        opening_hours: "Inexistente",
        photo: "Inexistente",
      };
    }
    return err.response.data;
  }
};

export default getStatus;
