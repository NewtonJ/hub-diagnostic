import axios from "axios";

const getAccessToken = async () => {
  const requestBody = {
    username: "username",
    password: "password",
  };

  const { data } = await axios.post(
    `${process.env.REACT_APP_AUTH_API}/signin`,
    requestBody
  );

  return data.accessToken;
};

export default getAccessToken;
