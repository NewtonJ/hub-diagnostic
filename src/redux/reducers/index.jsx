import { combineReducers } from "redux";
import UserInfoReducer from "./UserInfo";
import CompanyInfoReducer from "./CompanyInfo";
import DiagnosticReducer from "./Diagnostic";

export default combineReducers({
  UserInfoReducer,
  CompanyInfoReducer,
  DiagnosticReducer,
});
