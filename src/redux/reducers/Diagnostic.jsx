const initialState = {
  chartData: [],
  tableData: [],
};

export default function DiagnosticReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_CHART_DATA":
      return {
        ...state,
        chartData: action.payload,
      };
    case "SET_TABLE_DATA":
      return {
        ...state,
        tableData: action.payload,
      };
    default:
      return state;
  }
}
