const initialState = {
  username: "",
  email: "",
  wpp: "",
  confirmPassage: false,
};

export default function UserInfoReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_CONFIRM_PASSAGE":
      return { ...state, confirmPassage: true };
    case "SET_USER_DATA":
      return {
        ...state,
        username: action.payload.username,
        email: action.payload.email,
        wpp: action.payload.wpp,
      };
    default:
      return state;
  }
}
