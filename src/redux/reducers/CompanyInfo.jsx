const initialState = {
  numberOfAddresses: 0,
  companyForms: [],
  companyData: {},
  companyExistsInGoogle: {},
  companyExistsInYelp: {},
  companyExistsInFoursquare: {},
};

export default function CompanyInfoReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_NUMBER_OF_ADDRESSES":
      return {
        ...state,
        numberOfAddresses: action.payload,
      };
    case "SET_COMPANY_FORMS":
      return { ...state, companyForms: action.payload };
    case "SET_COMPANY_DATA":
      return { ...state, companyData: action.payload };
    case "SET_COMPANY_EXISTS_IN_GOOGLE":
      return { ...state, companyExistsInGoogle: action.payload };
    case "SET_COMPANY_EXISTS_IN_YELP":
      return { ...state, companyExistsInYelp: action.payload };
    case "SET_COMPANY_EXISTS_IN_FOURSQUARE":
      return { ...state, companyExistsInFoursquare: action.payload };
    default:
      return state;
  }
}
