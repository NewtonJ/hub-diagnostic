const styles = {
  buttonColor: { backgroundColor: "#FF4040" },
  buttonTextStyle: { padding: "0.5em", color: "#FFFFFF" },
};

export default styles;
