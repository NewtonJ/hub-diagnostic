import React from "react";
import { Button, Typography } from "@material-ui/core";

import styles from "./styles";

export default ({ onClick }) => (
  <Button style={styles.buttonColor} onClick={onClick}>
    <Typography style={styles.buttonTextStyle}>Próximo Passo</Typography>
  </Button>
);
