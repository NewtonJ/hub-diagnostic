import React from "react";
import { TextField } from "@material-ui/core";

import styles from "./styles";

export default ({
  placeholder,
  value,
  onChangeText,
  name,
  type = "text",
  fullWidth = false,
  helperText = "",
  error = false,
}) => (
  <TextField
    variant="outlined"
    placeholder={placeholder}
    name={name}
    type={type}
    fullWidth={fullWidth}
    value={value}
    onChange={onChangeText}
    error={error}
    helperText={error ? helperText : ""}
    style={styles.color}
  />
);
