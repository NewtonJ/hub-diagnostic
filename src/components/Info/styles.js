const styles = {
  divMargin: { marginTop: "5%", width: "100%" },
  gridMargin: { marginTop: "3%", marginBottom: "3%" },
  infoTextStyle: {
    fontFamily: "Poppins",
    fontSize: "1.5em",
    padding: 10,
    textAlign: "center",
    marginLeft: "5%",
    marginRight: "5%",
  },
  mobileInfoTextStyle: {
    fontFamily: "Poppins",
    fontSize: "1em",
    padding: 10,
    textAlign: "center",
    marginLeft: "5%",
    marginRight: "5%",
  },
};

export default styles;
