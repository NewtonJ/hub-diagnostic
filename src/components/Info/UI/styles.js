const styles = {
  divStyle: { display: "flex", flexDirection: "column", alignItems: "center" },
  iconStyle: { width: "4em", height: "4em" },
  mobileIconStyle: { width: "3em", height: "3em" },
  iconTextStyle: {
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: "1.2em",
  },
  mobileIconTextStyle: {
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: "0.8em",
  },
};

export default styles;
