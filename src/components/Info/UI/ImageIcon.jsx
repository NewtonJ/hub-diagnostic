import React from "react";
import RoundedImage from "react-rounded-image";
import { Typography } from "@material-ui/core";

import styles from "./styles";

const isMobile = window.innerWidth <= 500 || window.innerHeight <= 500;

export default ({ text, file }) => (
  <div style={styles.divStyle}>
    <img
      src={file}
      style={isMobile ? styles.mobileIconStyle : styles.iconStyle}
    />
    <Typography
      style={isMobile ? styles.mobileIconTextStyle : styles.iconTextStyle}
    >
      {text}
    </Typography>
  </div>
);
