import React from "react";
import { Typography, Grid } from "@material-ui/core";

import ImageIcon from "./UI/ImageIcon";
import googleMaps from "../../assets/images/Google Maps.png";
import waze from "../../assets/images/Waze.png";
import yelp from "../../assets/images/yelp.png";
import facebook from "../../assets/images/Facebook Places.png";
import whatsapp from "../../assets/images/Whatsapp.png";
import foursquare from "../../assets/images/Foursquare.png";
import linkedin from "../../assets/images/linkedin.png";

import styles from "./styles";

const isMobile = window.innerWidth <= 500 || window.innerHeight <= 500;

export default () => (
  <div style={styles.divMargin}>
    <Typography
      style={isMobile ? styles.mobileInfoTextStyle : styles.infoTextStyle}
    >
      Teste se sua empresa é encontrada nos principais mapas, listas e
      diretórios da internet
    </Typography>
    <Grid container justify="space-around" style={styles.gridMargin}>
      <ImageIcon text="Google Maps" file={googleMaps} />
      <ImageIcon text="Waze" file={waze} />
      <ImageIcon text="Facebook" file={facebook} />
      <ImageIcon text="Yelp" file={yelp} />
    </Grid>
    <Grid container justify="space-around">
      <ImageIcon text="Foursquare" file={foursquare} />
      <ImageIcon text="Whatsapp" file={whatsapp} />
      <ImageIcon text="Linkedin" file={linkedin} />
    </Grid>
  </div>
);
