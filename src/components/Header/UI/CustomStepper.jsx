import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { StepConnector, Grid } from "@material-ui/core";

import StepButton from "./StepButton";
import styles from "./styles";

const CustomConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  line: {
    height: 3,
    border: 0,
    borderRadius: 1,
  },
})(StepConnector);

const CustomStepper = () => {
  const history = useHistory();
  const [currentStep, setCurrentStep] = useState(1);

  const location = useLocation();

  useEffect(() => {
    const step = location.pathname.split("/")[2];
    setCurrentStep(parseInt(step));
    history.listen((location) => {
      const step = location.pathname.split("/")[2];
      setCurrentStep(parseInt(step));
    });
  });

  return (
    <div style={styles.stepperDivStyle}>
      <Grid container alignItems="center">
        <StepButton
          primaryColor="#4287f5"
          secondaryColor="#FFFFFF"
          text="1"
          isCurrentStep={currentStep === 1}
        />
        <CustomConnector style={styles.blueConnector} />
        <StepButton
          primaryColor="#4287f5"
          secondaryColor="#FFFFFF"
          text="2"
          isCurrentStep={currentStep === 2}
        />
        {/* <CustomConnector style={styles.yellowConnector} />
        <StepButton
          primaryColor="#fc382d"
          secondaryColor="#FFFFFF"
          text="3"
          isCurrentStep={currentStep === 3}
        />
        <CustomConnector style={styles.redConnector} />
        <StepButton
          primaryColor="#15c277"
          secondaryColor="#FFFFFF"
          text="4"
          isCurrentStep={currentStep === 4}
        /> */}
      </Grid>
    </div>
  );
};

export default CustomStepper;
