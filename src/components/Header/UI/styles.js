const styles = {
  titleDisplayStyle: {
    display: "contents",
  },
  mobileFontStyle: { fontSize: "1em", fontFamily: "Poppins" },
  fontStyle: { fontSize: "2em", fontFamily: "Poppins" },
  logoStyle: { height: "5em", width: "15em" },
  mobileLogoStyle: { height: "3em", width: "10em" },
  stepperDivStyle: { width: "30%", marginTop: "3%" },
  blueConnector: { backgroundColor: "#4287f5" },
  yellowConnector: { backgroundColor: "#f7db20" },
  redConnector: { backgroundColor: "#fc382d" },
};

export default styles;
