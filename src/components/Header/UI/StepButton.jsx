import React, { useState } from "react";
import { IconButton } from "@material-ui/core";

const StepButton = ({ primaryColor, secondaryColor, text, isCurrentStep }) => {
  const [hover, setHover] = useState(false);

  const setColor = (hover, isCurrentStep) => {
    if (isCurrentStep || hover) return secondaryColor;
    return primaryColor;
  };
  const setBackgroundColor = (hover, isCurrentStep) => {
    if (isCurrentStep || hover) return primaryColor;
    return secondaryColor;
  };

  return (
    <div>
      <IconButton
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        variant="outlined"
        style={{
          color: setColor(hover, isCurrentStep),
          backgroundColor: setBackgroundColor(hover, isCurrentStep),
        }}
      >
        {text}
      </IconButton>
    </div>
  );
};

export default StepButton;
