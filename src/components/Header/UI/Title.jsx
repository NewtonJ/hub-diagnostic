import React from "react";
import { Typography } from "@material-ui/core";

import logo from "../../../assets/images/logo-hub.png";
import styles from "./styles";

const isMobile = window.innerWidth <= 500 || window.innerHeight <= 500;

export default () => (
  <div style={styles.titleDisplayStyle}>
    <img
      src={logo}
      style={isMobile ? styles.mobileLogoStyle : styles.logoStyle}
      alt=""
    />
    <Typography style={isMobile ? styles.mobileFontStyle : styles.fontStyle}>
      Verifique sua presença online
    </Typography>
  </div>
);
