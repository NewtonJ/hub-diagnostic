import React from "react";

import Title from "./UI/Title";
import CustomStepper from "./UI/CustomStepper";
import styles from "./styles";

export default ({ children }) => (
  <div style={styles.mainDivStyle}>
    <Title />
    <CustomStepper />
    <div style={styles.contentDivMargin}>{children}</div>
  </div>
);
