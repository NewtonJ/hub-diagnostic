const styles = {
  mainDivStyle: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
  },
  contentDivMargin: {
    marginTop: "5%",
  },
};

export default styles;
