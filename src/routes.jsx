import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { PrivateRoute } from "./utils/PrivateRoute";
import { DiagnosticRoute } from "./utils/DiagnosticRoute";
import { Step1, Step2, Diagnostic, FormPage } from "./pages";

export default () => (
  <Switch>
    <Route exact path="/" component={FormPage} />
    {/* <Route exact path="/" render={() => <Redirect to="/step/1" />} /> */}
    <DiagnosticRoute exact path="/diagnostic" component={Diagnostic} />
    {/* <Route exact path="/step/1" component={Step1} />
    <PrivateRoute exact path="/step/2" component={Step2} /> */}
  </Switch>
);
