import store from "../redux/index";

export const isThereDiagnosticData = () => {
  const {
    DiagnosticReducer: { chartData },
  } = store.getState();
  return chartData.length > 0;
};
