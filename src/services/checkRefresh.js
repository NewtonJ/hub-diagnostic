import store from "../redux/index";

export const didNotRefreshPage = () => {
  const {
    CompanyInfoReducer: { companyData },
  } = store.getState();
  return companyData.companyName;
};
