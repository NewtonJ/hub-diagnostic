import React from "react";
import { Grid } from "@material-ui/core";
import InputMask from "react-input-mask";

import { TextInput } from "../../../components";
import StateSelect from "./StateSelect";

const DataForm = ({
  isMobile,
  companyName,
  companyAddress,
  companyAddressNumber,
  city,
  state,
  phone,
  errorCompanyName,
  errorCompanyAddress,
  errorCompanyAddressNumber,
  errorCity,
  errorState,
  errorPhone,
  handleChangeForm,
  handleChangeCEP,
}) => {
  const renderResponsive = () => {
    if (isMobile) {
      return (
        <>
          <Grid item>
            <TextInput
              placeholder="Nome da Empresa"
              value={companyName}
              name="companyName"
              onChangeText={handleChangeForm}
              error={errorCompanyName}
              fullWidth
            />
          </Grid>
          <Grid item>
            <TextInput
              placeholder="CEP"
              //value={form.zipCode}
              type="number"
              name="zipCode"
              onChangeText={handleChangeCEP}
            />
          </Grid>
          <Grid item>
            <TextInput
              fullWidth
              placeholder="Rua"
              value={companyAddress}
              name="companyAddress"
              onChangeText={handleChangeForm}
              error={errorCompanyAddress}
            />
          </Grid>
          <Grid item>
            <TextInput
              placeholder="Nº"
              type="number"
              value={companyAddressNumber}
              name="companyAddressNumber"
              onChangeText={handleChangeForm}
              error={errorCompanyAddressNumber}
            />
          </Grid>
          <Grid item>
            <TextInput
              placeholder="Cidade"
              value={city}
              name="city"
              onChangeText={handleChangeForm}
              error={errorCity}
            />
          </Grid>
          <Grid item>
            <StateSelect
              value={state}
              name="state"
              onChange={handleChangeForm}
              error={errorState}
            />
          </Grid>
          <Grid item>
            <InputMask mask="(99)999999999" maskChar={""} value={phone}>
              {() => (
                <TextInput
                  placeholder="Telefone Principal"
                  type="tel"
                  value={phone}
                  name="phone"
                  onChangeText={handleChangeForm}
                  error={errorPhone}
                />
              )}
            </InputMask>
          </Grid>
        </>
      );
    } else {
      return (
        <Grid item>
          <Grid container spacing={1}>
            <Grid item xs={8}>
              <TextInput
                placeholder="Nome da Empresa"
                value={companyName}
                name="companyName"
                onChangeText={handleChangeForm}
                error={errorCompanyName}
                fullWidth
              />
            </Grid>
            <Grid item xs={3}>
              <TextInput
                placeholder="CEP"
                //value={form.zipCode}
                type="number"
                name="zipCode"
                onChangeText={handleChangeCEP}
              />
            </Grid>
          </Grid>
          <Grid container spacing={1}>
            <Grid item xs={4}>
              <TextInput
                fullWidth
                placeholder="Rua"
                value={companyAddress}
                name="companyAddress"
                onChangeText={handleChangeForm}
                error={errorCompanyAddress}
              />
            </Grid>
            <Grid item xs={2}>
              <TextInput
                placeholder="Nº"
                type="number"
                value={companyAddressNumber}
                name="companyAddressNumber"
                onChangeText={handleChangeForm}
                error={errorCompanyAddressNumber}
              />
            </Grid>
            <Grid item xs={2}>
              <TextInput
                placeholder="Cidade"
                value={city}
                name="city"
                onChangeText={handleChangeForm}
                error={errorCity}
              />
            </Grid>
            <Grid item xs={2}>
              <StateSelect
                value={state}
                name="state"
                onChange={handleChangeForm}
                error={errorState}
              />
            </Grid>
            <Grid item xs={3}>
              {/* <TextInput
                placeholder="Telefone Principal"
                type="phone"
                value={phone}
                name="phone"
                onChangeText={handleChangeForm}
                error={errorPhone}
              /> */}
              <InputMask mask="(99)999999999" maskChar={""} value={phone}>
                {() => (
                  <TextInput
                    placeholder="Telefone Principal"
                    type="phone"
                    value={phone}
                    name="phone"
                    onChangeText={handleChangeForm}
                    error={errorPhone}
                  />
                )}
              </InputMask>
            </Grid>
          </Grid>
        </Grid>
      );
    }
  };

  return renderResponsive();
};

export default DataForm;
