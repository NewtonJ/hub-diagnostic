import React, { useState } from "react";
import { Grid, Box, Paper, Typography } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import axios from "axios";
import queryString from "query-string";

import { ProgressButton, Info, Header } from "../../components";
import DataForm from "./UI/DataForm";
import styles from "./styles";

const isMobile = window.innerWidth <= 500 || window.innerHeight <= 500;

const Step1 = () => {
  const dispatch = useDispatch();
  const query = queryString.parse(window.location.search);
  const history = useHistory();
  const [form, setState] = useState({
    companyName: query.companyName
      ? query.companyName
      : "Farmácias Pague Menos",
    zipCode: "",
    companyAddress: query.companyAddress
      ? query.companyAddress
      : "Avenida Francisco Sá",
    companyAddressNumber: query.companyAddressNumber
      ? query.companyAddressNumber
      : "4475",
    city: query.city ? query.city : "Fortaleza",
    state: query.state ? query.state : "CE",
    phone: query.phone ? query.phone : "8534338633",
  });
  const [errorCompanyName, setErrorCompanyName] = useState(false);
  const [errorCompanyAddress, setErrorCompanyAddress] = useState(false);
  const [errorCompanyAddressNumber, setErrorCompanyAddressNumber] = useState(
    false
  );
  const [errorCity, setErrorCity] = useState(false);
  const [errorState, setErrorState] = useState(false);
  const [errorPhone, setErrorPhone] = useState(false);

  const handleChangeForm = ({ target: { name, value } }) => {
    setState({ ...form, [name]: value });
  };

  const handleChangeCEP = async ({ target: { value } }) => {
    if (value.toString().length === 8) {
      const { data } = await axios.get(
        `https://viacep.com.br/ws/${value}/json/`
      );
      setState({
        ...form,
        companyAddress: data.logradouro,
        city: data.localidade,
        state: data.uf,
      });
    }
  };

  const validate = () => {
    let isValid = true;
    if (form.companyName === "") {
      setErrorCompanyName(true);
      isValid = false;
    }
    if (form.companyAddress === "") {
      setErrorCompanyAddress(true);
      isValid = false;
    }
    if (form.companyAddressNumber === "") {
      setErrorCompanyAddressNumber(true);
      isValid = false;
    }
    if (form.city === "") {
      setErrorCity(true);
      isValid = false;
    }
    if (form.state === "") {
      setErrorState(true);
      isValid = false;
    }
    if (form.phone === "") {
      setErrorPhone(true);
      isValid = false;
    }
    return isValid;
  };

  const handleButtonClick = () => {
    dispatch({
      type: "SET_COMPANY_DATA",
      payload: form,
    });
    if (validate()) history.push("/step/2");
    else alert("Preencha todos os campos necessários");
  };

  return (
    <div>
      <Header />
      <Grid container direction="column" alignItems="flex-start">
        <Grid
          container
          direction="row"
          justify="flex-start"
          style={styles.textGridStyle}
        >
          <Typography>
            <Box fontWeight="fontWeightBold" fontFamily="Poppins" m={0.5}>
              [1/3] Primeiro Passo:
            </Box>
          </Typography>
          <Typography>
            <Box fontWeight="fontWeightRegular" fontFamily="Poppins" m={0.5}>
              conte-nos sobre sua empresa
            </Box>
          </Typography>
        </Grid>
        <Paper elevation={0}>
          <Grid
            container
            spacing={2}
            direction="column"
            style={styles.contentGridStyle}
          >
            <DataForm
              isMobile={isMobile}
              companyName={form.companyName}
              companyAddress={form.companyAddress}
              companyAddressNumber={form.companyAddressNumber}
              city={form.city}
              state={form.state}
              phone={form.phone}
              handleChangeForm={handleChangeForm}
              handleChangeCEP={handleChangeCEP}
              errorCompanyName={errorCompanyName}
              errorCompanyAddress={errorCompanyAddress}
              errorCompanyAddressNumber={errorCompanyAddressNumber}
              errorCity={errorCity}
              errorState={errorState}
              errorPhone={errorPhone}
            />

            <Grid container justify="center">
              <ProgressButton onClick={handleButtonClick} />
            </Grid>
          </Grid>
          <Info />
        </Paper>
      </Grid>
    </div>
  );
};

export default Step1;
