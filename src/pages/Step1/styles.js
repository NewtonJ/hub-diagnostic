const styles = {
  textGridStyle: {
    padding: "0.5em",
    backgroundColor: "#DBDBDB",
    marginLeft: "0",
    width: "auto",
    marginBottom: "0.28em",
  },
  contentGridStyle: {
    width: "96vw",
    marginLeft: "0.1%",
    padding: "0.5em",
    backgroundColor: "#00A6FF",
  },
};

export default styles;
