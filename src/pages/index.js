import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import FormPage from "./FormPage";
import Diagnostic from "./Diagnostic";

export { Step1, Step2, Step3, Diagnostic, FormPage };
