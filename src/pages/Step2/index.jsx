import React, { useState } from "react";
import {
  Grid,
  Paper,
  Typography,
  Box,
  CircularProgress,
  Button,
} from "@material-ui/core";
import BlockUi from "react-block-ui";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import InputMask from "react-input-mask";

import "./blockStyle.css";
import { TextInput, ProgressButton, Info, Header } from "../../components";
import styles from "./styles";

import getAccessToken from "../../utils/getAccessToken";
import getStatus from "../../utils/getStatus";
import createChartData from "../../utils/createChartData";
import createTableData from "../../utils/createTableData";
import getPlaceInfo from "../../utils/getPlaceInfo";
import formatPhone from "../../utils/formatPhone";
import saveLead from "../../utils/saveLead";

const isMobile = window.innerWidth <= 500 || window.innerHeight <= 500;

const Step2 = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [form, setState] = useState({
    username: "",
    userEmail: "",
    wpp: "",
  });

  const [errorName, setErrorName] = useState(false);
  const [errorEmail, setErrorEmail] = useState(false);
  const [errorWpp, setErrorWpp] = useState(false);

  const companyData = useSelector(
    (state) => state.CompanyInfoReducer.companyData
  );

  const handleChangeForm = ({ target: { name, value } }) => {
    setState({ ...form, [name]: value });
  };

  const isValid = () => {
    let isValid = true;
    if (form.username === "") {
      setErrorName(true);
      isValid = false;
    }
    if (form.userEmail === "") {
      setErrorEmail(true);
      isValid = false;
    }
    if (form.wpp === "") {
      setErrorWpp(true);
      isValid = false;
    }
    return isValid;
  };

  const handleTryAgainClick = () => {
    history.push("/step/1");
  };

  const renderResponsive = () => {
    if (isMobile) {
      return (
        <Grid
          container
          spacing={3}
          direction="column"
          style={styles.contentGridStyle}
        >
          <Grid item>
            <TextInput
              placeholder="Seu nome"
              value={form.username}
              name="username"
              onChangeText={handleChangeForm}
              error={errorName}
              fullWidth
            />
          </Grid>
          <Grid item>
            <TextInput
              placeholder="Seu e-mail"
              value={form.userEmail}
              name="userEmail"
              onChangeText={handleChangeForm}
              error={errorEmail}
              fullWidth
            />
          </Grid>
          <Grid item>
            <InputMask mask="(99)999999999" maskChar={""} value={form.wpp}>
              {() => (
                <TextInput
                  placeholder="Seu Whatsapp"
                  value={form.wpp}
                  name="wpp"
                  onChangeText={handleChangeForm}
                  error={errorWpp}
                  fullWidth
                />
              )}
            </InputMask>
          </Grid>

          <Grid item>
            <ProgressButton onClick={handleButtonClick} />
          </Grid>
        </Grid>
      );
    }
    return (
      <Grid
        container
        spacing={3}
        alignItems="center"
        justify="space-between"
        style={styles.contentGridStyle}
      >
        <Grid item>
          <Grid container spacing={1} alignItems="center">
            <Grid item>
              <TextInput
                placeholder="Seu nome"
                value={form.username}
                name="username"
                onChangeText={handleChangeForm}
                error={errorName}
              />
            </Grid>
            <Grid item>
              <TextInput
                placeholder="Seu e-mail"
                value={form.userEmail}
                name="userEmail"
                onChangeText={handleChangeForm}
                error={errorEmail}
              />
            </Grid>
            <Grid item>
              <InputMask mask="(99)999999999" maskChar={""} value={form.wpp}>
                {() => (
                  <TextInput
                    placeholder="Seu Whatsapp"
                    value={form.wpp}
                    name="wpp"
                    onChangeText={handleChangeForm}
                    error={errorWpp}
                  />
                )}
              </InputMask>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <ProgressButton onClick={handleButtonClick} />
        </Grid>
      </Grid>
    );
  };

  const renderLoadingScreen = () => (
    <div>
      <CircularProgress style={styles.loadingColor} />
      <Typography style={styles.loadingTitleStyle}>
        Por favor aguarde!
      </Typography>
      <Typography style={styles.loadingTextStyle}>
        Essa pesquisa demora no máximo 30 segundos para carregar
      </Typography>
      <Typography style={styles.loadingTextStyle}>
        Se em 30 segundos você não for redirecionado, clique no botão abaixo
      </Typography>
      <Button style={styles.loadingButtonStyle} onClick={handleTryAgainClick}>
        <Typography style={styles.loadingButtonTextStyle}>
          Tente Novamente
        </Typography>
      </Button>
    </div>
  );

  const handleButtonClick = async () => {
    console.log(companyData);
    if (!isValid()) {
      alert("Preencha todos os campos necessários");
      return null;
    }
    setLoading(true);

    const {
      companyName,
      companyAddress,
      companyAddressNumber,
      city,
      state,
      phone,
    } = companyData;
    const formattedPhone = formatPhone(phone);
    dispatch({
      type: "SET_USER_DATA",
      payload: {
        username: form.username,
        email: form.userEmail,
        wpp: form.wpp,
      },
    });
    dispatch({ type: "SET_CONFIRM_PASSAGE" });
    const accessToken = await getAccessToken();

    const googlePlacesResults = await getStatus(
      accessToken,
      "GOOGLE_PLACES",
      companyName,
      companyAddress,
      companyAddressNumber,
      city,
      state,
      formattedPhone
    );
    const yelpResults = await getStatus(
      accessToken,
      "YELP",
      companyName,
      companyAddress,
      companyAddressNumber,
      city,
      state,
      formattedPhone
    );
    const foursquareResults = await getStatus(
      accessToken,
      "FOURSQUARE",
      companyName,
      companyAddress,
      companyAddressNumber,
      city,
      state,
      formattedPhone
    );
    const googlePlacesInfo = await getPlaceInfo(
      "GOOGLE_PLACES",
      accessToken,
      companyName,
      `${companyAddress}, ${companyAddressNumber}, ${city}, ${state}`
    );
    const yelpInfo = await getPlaceInfo(
      "YELP",
      accessToken,
      companyName,
      `${companyAddress}, ${companyAddressNumber}, ${city}, ${state}`
    );
    const foursquareInfo = await getPlaceInfo(
      "FOURSQUARE",
      accessToken,
      companyName,
      `${companyAddress}, ${companyAddressNumber}, ${city}, ${state}`
    );
    const chartData = createChartData(
      googlePlacesResults,
      yelpResults,
      foursquareResults
    );
    const tableData = createTableData(
      googlePlacesResults,
      googlePlacesInfo,
      yelpResults,
      yelpInfo,
      foursquareResults,
      foursquareInfo
    );

    const correctPercentage =
      chartData[1][1] / (chartData[1][1] + chartData[2][1] + chartData[3][1]);
    const unexistingPercentage =
      chartData[2][1] / (chartData[1][1] + chartData[2][1] + chartData[3][1]);
    const incorrectPercentage =
      chartData[3][1] / (chartData[1][1] + chartData[2][1] + chartData[3][1]);

    const lead = await saveLead(
      form.username,
      form.userEmail,
      formatPhone(form.wpp),
      companyName,
      companyAddress,
      companyAddressNumber,
      city,
      state,
      phone,
      correctPercentage,
      incorrectPercentage,
      unexistingPercentage,
      accessToken
    );

    dispatch({ type: "SET_CHART_DATA", payload: chartData });
    dispatch({ type: "SET_TABLE_DATA", payload: tableData });
    history.push("/diagnostic");
  };

  return (
    <div>
      <BlockUi tag="div" blocking={loading} loader={renderLoadingScreen}>
        <Header />
        <Grid container direction="column" alignItems="center">
          <Grid
            container
            direction="row"
            justify="flex-start"
            style={styles.textGridStyle}
          >
            <Typography>
              <Box fontWeight="fontWeightBold" fontFamily="Poppins" m={0.5}>
                [2/3] Segundo Passo:
              </Box>
            </Typography>
            <Typography>
              <Box fontWeight="fontWeightRegular" fontFamily="Poppins" m={0.5}>
                conte-nos sobre você
              </Box>
            </Typography>
          </Grid>
          <Paper elevation={0}>
            {renderResponsive()}
            <Info />
          </Paper>
        </Grid>
      </BlockUi>
    </div>
  );
};

export default Step2;
