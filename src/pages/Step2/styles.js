const styles = {
  textGridStyle: {
    padding: "0.5em",
    backgroundColor: "#DBDBDB",
    marginLeft: "0",
    width: "auto",
    marginBottom: "0.7em",
  },
  contentGridStyle: {
    marginLeft: "0.1%",
    padding: "0.5em",
    backgroundColor: "#00A6FF",
  },
  loadingColor: { color: "#FF4040" },
  loadingTitleStyle: {
    fontSize: "70px",
    letterSpacing: "-0.1em",
    color: "#00A6FF",
  },
  loadingTextStyle: {
    fontSize: "20px",
    color: "#00A6FF",
    marginBottom: "0.5em",
  },
  loadingButtonStyle: {
    backgroundColor: "#00A6FF",
    marginTop: "0.5em",
  },
  loadingButtonTextStyle: {
    fontSize: "15px",
    color: "#FFFFFF",
  },
};

export default styles;
