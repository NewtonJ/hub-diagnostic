import React from "react";
import { Grid, Button } from "@material-ui/core";
import ReCAPTCHA from "react-google-recaptcha";
import { isMobile } from "react-device-detect";
import MaskedInput from "react-maskedinput";
import InputMask from "react-input-mask";

import StateSelect from "./StateSelect";
import styles from "./styles";
import mobileStyles from "./mobileStyles";

const siteKey = process.env.REACT_APP_GOOGLE_CAPTCHA_SITE_KEY;

const Form = ({
  handleChangeForm,
  handleChangeCep,
  companyName,
  errorCompanyName,
  zipCode,
  errorZipCode,
  companyAddressNumber,
  errorCompanyAddressNumber,
  companyAddress,
  errorCompanyAddress,
  city,
  errorCity,
  state,
  errorState,
  companyPhone,
  errorCompanyPhone,
  whatsapp,
  errorWhatsapp,
  userName,
  errorUserName,
  userEmail,
  errorUserEmail,
  captcha,
  onCaptchaChange,
  onButtonClick,
}) => (
  <Grid
    container
    direction="column"
    spacing={1}
    style={isMobile ? {} : { marginLeft: "3em" }}
    alignItems={isMobile ? "center" : ""}
  >
    <Grid item>
      <input
        placeholder="Nome da Empresa"
        style={
          errorCompanyName
            ? isMobile
              ? mobileStyles.bigInputErrorStyle
              : styles.bigInputErrorStyle
            : isMobile
            ? mobileStyles.bigInputStyle
            : styles.bigInputStyle
        }
        name="companyName"
        value={companyName}
        onChange={handleChangeForm}
      />
    </Grid>
    <Grid item>
      <Grid container spacing={1} direction="row">
        <Grid item>
          <InputMask
            value={zipCode}
            onChange={handleChangeCep}
            mask="99999-999"
            maskChar={""}
          >
            {() => (
              <input
                placeholder="CEP"
                style={
                  errorZipCode
                    ? isMobile
                      ? mobileStyles.smallInputErrorStyle
                      : styles.smallInputErrorStyle
                    : isMobile
                    ? mobileStyles.smallInputStyle
                    : styles.smallInputStyle
                }
                name="zipCode"
              />
            )}
          </InputMask>
        </Grid>
        <Grid item>
          <input
            placeholder="Nº"
            style={
              errorCompanyAddressNumber
                ? isMobile
                  ? mobileStyles.smallerInputErrorStyle
                  : styles.smallerInputErrorStyle
                : isMobile
                ? mobileStyles.smallerInputStyle
                : styles.smallerInputStyle
            }
            name="companyAddressNumber"
            value={companyAddressNumber}
            onChange={handleChangeForm}
          />
        </Grid>
      </Grid>
    </Grid>
    <Grid item>
      <input
        placeholder="Endereço"
        style={
          errorCompanyAddress
            ? isMobile
              ? mobileStyles.bigInputErrorStyle
              : styles.bigInputErrorStyle
            : isMobile
            ? mobileStyles.bigInputStyle
            : styles.bigInputStyle
        }
        name="companyAddress"
        value={companyAddress}
        onChange={handleChangeForm}
      />
    </Grid>
    <Grid item>
      <Grid container spacing={1} direction="row">
        <Grid item>
          <input
            placeholder="Cidade"
            style={
              errorCity
                ? isMobile
                  ? mobileStyles.smallInputErrorStyle
                  : styles.smallInputErrorStyle
                : isMobile
                ? mobileStyles.smallInputStyle
                : styles.smallInputStyle
            }
            name="city"
            value={city}
            onChange={handleChangeForm}
          />
        </Grid>
        <Grid item>
          <StateSelect
            name="state"
            value={state}
            onChange={handleChangeForm}
            error={errorState}
          />
        </Grid>
      </Grid>
    </Grid>
    <Grid item>
      <Grid container spacing={1} direction="row">
        <Grid item>
          <InputMask
            value={companyPhone}
            mask="(99)999999999"
            maskChar={""}
            onChange={handleChangeForm}
          >
            {() => (
              <input
                placeholder="Telefone Principal"
                style={
                  errorCompanyPhone
                    ? isMobile
                      ? mobileStyles.smallInputErrorStyle
                      : styles.smallInputErrorStyle
                    : isMobile
                    ? mobileStyles.smallInputStyle
                    : styles.smallInputStyle
                }
                name="companyPhone"
              />
            )}
          </InputMask>
        </Grid>
        <Grid item>
          <InputMask
            value={whatsapp}
            mask="(99)999999999"
            maskChar={""}
            onChange={handleChangeForm}
          >
            {() => (
              <input
                placeholder="Seu Whatsapp"
                style={
                  errorWhatsapp
                    ? isMobile
                      ? mobileStyles.smallerInputErrorStyle
                      : styles.smallerInputErrorStyle
                    : isMobile
                    ? mobileStyles.smallerInputStyle
                    : styles.smallerInputStyle
                }
                name="whatsapp"
              />
            )}
          </InputMask>
        </Grid>
      </Grid>
    </Grid>
    <Grid item>
      <input
        placeholder="Seu Nome"
        style={
          errorUserName
            ? isMobile
              ? mobileStyles.bigInputErrorStyle
              : styles.bigInputErrorStyle
            : isMobile
            ? mobileStyles.bigInputStyle
            : styles.bigInputStyle
        }
        name="userName"
        value={userName}
        onChange={handleChangeForm}
      />
    </Grid>
    <Grid item>
      <input
        placeholder="Seu Email"
        style={
          errorUserEmail
            ? isMobile
              ? mobileStyles.bigInputErrorStyle
              : styles.bigInputErrorStyle
            : isMobile
            ? mobileStyles.bigInputStyle
            : styles.bigInputStyle
        }
        name="userEmail"
        value={userEmail}
        onChange={handleChangeForm}
      />
    </Grid>
    <Grid item>
      <ReCAPTCHA sitekey={siteKey} onChange={onCaptchaChange} />
    </Grid>
    <Grid item>
      <Button
        onClick={onButtonClick}
        disabled={captcha === ""}
        style={{
          width: "14em",
          fontFamily: "Poppins",
          fontWeight: "bold",
          color: "white",
          borderRadius: "1em",
          backgroundColor: "#5BC835",
        }}
      >
        SOLICITAR DIAGNÓSTICO
      </Button>
    </Grid>
  </Grid>
);

export default Form;
