import React from "react";
import { isMobile } from "react-device-detect";
import { Select, FormControl } from "@material-ui/core";

export default ({ value, onChange, name, error }) => (
  <select
    value={value}
    name={name}
    onChange={onChange}
    style={
      error
        ? isMobile
          ? {
              fontFamily: "Poppins",
              borderColor: "#fc382d",
              width: "11em",
              padding: "0.4em",
              height: "2.5em",
              borderRadius: "0.5em",
              outline: "none",
              borderWidth: "0.1em",
            }
          : {
              fontFamily: "Poppins",
              borderColor: "#fc382d",
              width: "10em",
              padding: "0.4em",
              height: "2.5em",
              borderRadius: "0.5em",
              outline: "none",
              borderWidth: "0.01em",
            }
        : isMobile
        ? {
            fontFamily: "Poppins",
            borderColor: "#4287f5",
            width: "11em",
            padding: "0.4em",
            height: "2.5em",
            borderRadius: "0.5em",
            outline: "none",
            borderWidth: "0.1em",
          }
        : {
            fontFamily: "Poppins",
            borderColor: "#4287f5",
            width: "10em",
            padding: "0.4em",
            height: "2.5em",
            borderRadius: "0.5em",
            outline: "none",
            borderWidth: "0.01em",
          }
    }
  >
    <option>Estado</option>
    <option value={"AC"}>AC</option>
    <option value={"AL"}>AL</option>
    <option value={"AP"}>AP</option>
    <option value={"AM"}>AM</option>
    <option value={"BA"}>BA</option>
    <option value={"CE"}>CE</option>
    <option value={"DF"}>DF</option>
    <option value={"ES"}>ES</option>
    <option value={"GO"}>GO</option>
    <option value={"MA"}>MA</option>
    <option value={"MT"}>MT</option>
    <option value={"MS"}>MS</option>
    <option value={"MG"}>MG</option>
    <option value={"PA"}>PA</option>
    <option value={"PB"}>PB</option>
    <option value={"PR"}>PR</option>
    <option value={"PE"}>PE</option>
    <option value={"PI"}>PI</option>
    <option value={"RJ"}>RJ</option>
    <option value={"RN"}>RN</option>
    <option value={"RS"}>RS</option>
    <option value={"RO"}>RO</option>
    <option value={"RR"}>RR</option>
    <option value={"SC"}>SC</option>
    <option value={"SP"}>SP</option>
    <option value={"SE"}>SE</option>
    <option value={"TO"}>TO</option>
  </select>
);
