const styles = {
  divStyle: {
    marginLeft: "0.8em",
    display: "flex",
    flexDirection: "column",
    marginTop: "1em",
  },
  mobileDivStyle: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  textStyle: { fontFamily: "Poppins", fontSize: "1.1em" },
  mobileTextStyle: { fontFamily: "Poppins", fontSize: "0.8em" },
};

export default styles;
