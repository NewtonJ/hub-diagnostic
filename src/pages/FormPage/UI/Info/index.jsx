import React from "react";
import { Typography } from "@material-ui/core";

import styles from "./styles";

const isMobile = window.innerWidth <= 500 || window.innerHeight <= 500;

export default () => (
  <div style={isMobile ? styles.mobileDivStyle : styles.divStyle}>
    <Typography style={isMobile ? styles.mobileTextStyle : styles.textStyle}>
      Teste se sua empresa é encontrada nos principais
    </Typography>
    <Typography style={isMobile ? styles.mobileTextStyle : styles.textStyle}>
      mapas, listas e diretórios da internet
    </Typography>
  </div>
);
