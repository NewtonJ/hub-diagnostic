const styles = {
  logoStyle: { height: "3em", width: "9em" },
  mobileLogoStyle: { height: "2em", width: "6em" },
  textStyle: {
    fontFamily: "Poppins",
    color: "#4287f5",
    fontSize: "1.5em",
    fontWeight: "bold",
  },
  mobileTextStyle: {
    fontFamily: "Poppins",
    color: "#4287f5",
    fontSize: "0.7em",
    fontWeight: "bold",
  },
};

export default styles;
