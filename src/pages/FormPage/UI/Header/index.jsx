import React from "react";
import { Grid, Typography } from "@material-ui/core";

import styles from "./styles";
import logoHub from "../../../../assets/images/logo-hub.png";

const isMobile = window.innerWidth <= 500 || window.innerHeight <= 500;

export default () => (
  <div>
    <Grid
      container
      alignItems="center"
      spacing={isMobile ? 1 : 2}
      justify="center"
    >
      <Grid item>
        <img
          src={logoHub}
          style={isMobile ? styles.mobileLogoStyle : styles.logoStyle}
        />
      </Grid>
      <Grid item>
        <Typography
          style={isMobile ? styles.mobileTextStyle : styles.textStyle}
        >
          |
        </Typography>
      </Grid>
      <Grid item>
        <Typography
          style={isMobile ? styles.mobileTextStyle : styles.textStyle}
        >
          VERIFIQUE SUA PRESENÇA ONLINE
        </Typography>
      </Grid>
    </Grid>
  </div>
);
