import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import BlockUi from "react-block-ui";
import { CircularProgress, Typography, Button } from "@material-ui/core";
import queryString from "query-string";
import { isMobile } from "react-device-detect";

import "./blockStyle.css";
import Header from "./UI/Header";
import Info from "./UI/Info";
import Form from "./UI/Form";
import background from "../../assets/images/Background-back.png";
import backgroundFront from "../../assets/images/Background-front.png";
import centralizedBackground from "../../assets/images/centralized-background.png";
import styles from "./styles";

import getAccessToken from "../../utils/getAccessToken";
import getStatus from "../../utils/getStatus";
import createChartData from "../../utils/createChartData";
import createTableData from "../../utils/createTableData";
import getPlaceInfo from "../../utils/getPlaceInfo";
import formatPhone from "../../utils/formatPhone";
import saveLead from "../../utils/saveLead";
import writeNumericAddress from "../../utils/writeNumericAddress";
import checkIfPlaceExists from "../../utils/checkIfPlaceExists";

const FormPage = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const query = queryString.parse(window.location.search);
  const [loading, setLoading] = useState(false);
  const [form, setState] = useState({
    companyName: query.companyName ? query.companyName : "",
    zipCode: query.zipCode ? query.zipCode : "",
    companyAddressNumber: query.companyAddressNumber
      ? query.companyAddressNumber
      : "",
    companyAddress: query.companyAddress ? query.companyAddress : "",
    city: query.city ? query.city : "",
    state: query.state ? query.state : "",
    companyPhone: query.companyPhone ? query.companyPhone : "",
    whatsapp: query.whatsapp ? query.whatsapp : "",
    userName: query.userName ? query.userName : "",
    userEmail: query.userEmail ? query.userEmail : "",
    captcha: "",
  });

  const [errorCompanyName, setErrorCompanyName] = useState(false);
  const [errorZipCode, setErrorZipCode] = useState(false);
  const [errorCompanyAddressNumber, setErrorCompanyAddressNumber] = useState(
    false
  );
  const [errorCompanyAddress, setErrorCompanyAddress] = useState(false);
  const [errorCity, setErrorCity] = useState(false);
  const [errorState, setErrorState] = useState(false);
  const [errorCompanyPhone, setErrorCompanyPhone] = useState(false);
  const [errorWhatsapp, setErrorWhatsapp] = useState(false);
  const [errorUserName, setErrorUserName] = useState(false);
  const [errorUserEmail, setErrorUserEmail] = useState(false);

  const handleChangeForm = ({ target: { value, name } }) => {
    setState({ ...form, [name]: value });
  };

  const handleChangeCEP = async ({ target: { value } }) => {
    setState({ ...form, zipCode: value });
    if (value.toString().length > 7) {
      const { data } = await axios.get(
        `https://viacep.com.br/ws/${value}/json/`
      );
      setState({
        ...form,
        zipCode: value,
        companyAddress: data.logradouro,
        city: data.localidade,
        state: data.uf,
      });
    }
  };

  const renderLoadingScreen = () => (
    <div>
      <CircularProgress style={styles.loadingColor} />
      <Typography style={styles.loadingTitleStyle}>
        Por favor aguarde!
      </Typography>
      <Typography style={styles.loadingTextStyle}>
        Essa pesquisa demora no máximo 30 segundos para carregar
      </Typography>
      <Typography style={styles.loadingTextStyle}>
        Se em 30 segundos você não for redirecionado, clique no botão abaixo
      </Typography>
      <Button
        style={styles.loadingButtonStyle}
        onClick={() => {
          history.push("/");
        }}
      >
        <Typography style={styles.loadingButtonTextStyle}>
          Tente Novamente
        </Typography>
      </Button>
    </div>
  );

  const validate = () => {
    let isValid = true;
    if (form.companyName === "") {
      setErrorCompanyName(true);
      isValid = false;
    }
    if (form.zipCode === "") {
      setErrorZipCode(true);
      isValid = false;
    }
    if (form.companyAddressNumber === "") {
      setErrorCompanyAddressNumber(true);
      isValid = false;
    }
    if (form.companyAddress === "") {
      setErrorCompanyAddress(true);
      isValid = false;
    }
    if (form.city === "") {
      setErrorCity(true);
      isValid = false;
    }
    if (form.state === "") {
      setErrorState(true);
      isValid = false;
    }
    if (form.companyPhone === "") {
      setErrorCompanyPhone(true);
      isValid = false;
    }
    if (form.whatsapp === "") {
      setErrorWhatsapp(true);
      isValid = false;
    }
    if (form.userName === "") {
      setErrorUserName(true);
      isValid = false;
    }
    if (form.userEmail === "") {
      setErrorUserEmail(true);
      isValid = false;
    }
    return isValid;
  };

  const onButtonClick = async () => {
    const isValid = validate();
    if (!isValid) {
      alert("Preencha todos os Campos");
      return false;
    }
    setLoading(true);
    const {
      companyName,
      companyAddress,
      companyAddressNumber,
      city,
      state,
      companyPhone,
      whatsapp,
      userName,
      userEmail,
      zipCode,
    } = form;
    const formattedAddress = writeNumericAddress(companyAddress);
    const formattedPhone = formatPhone(companyPhone);
    dispatch({
      type: "SET_COMPANY_DATA",
      payload: form,
    });
    dispatch({ type: "SET_CONFIRM_PASSAGE" });
    const accessToken = await getAccessToken();

    const companyExistsInGoogle = await checkIfPlaceExists(
      companyName,
      city,
      state,
      "GOOGLE_PLACES",
      accessToken
    );
    const companyExistsInYelp = await checkIfPlaceExists(
      companyName,
      city,
      state,
      "YELP",
      accessToken
    );
    const companyExistsInFoursquare = await checkIfPlaceExists(
      companyName,
      city,
      state,
      "FOURSQUARE",
      accessToken
    );

    dispatch({
      type: "SET_COMPANY_EXISTS_IN_GOOGLE",
      payload: companyExistsInGoogle,
    });
    dispatch({
      type: "SET_COMPANY_EXISTS_IN_YELP",
      payload: companyExistsInYelp,
    });
    dispatch({
      type: "SET_COMPANY_EXISTS_IN_FOURSQUARE",
      payload: companyExistsInFoursquare,
    });

    const googlePlacesResults = await getStatus(
      accessToken,
      "GOOGLE_PLACES",
      companyName,
      formattedAddress,
      companyAddressNumber,
      city,
      state,
      formattedPhone,
      zipCode
    );

    const yelpResults = await getStatus(
      accessToken,
      "YELP",
      companyName,
      formattedAddress,
      companyAddressNumber,
      city,
      state,
      formattedPhone,
      zipCode
    );
    const foursquareResults = await getStatus(
      accessToken,
      "FOURSQUARE",
      companyName,
      formattedAddress,
      companyAddressNumber,
      city,
      state,
      formattedPhone,
      zipCode
    );
    const googlePlacesInfo = await getPlaceInfo(
      "GOOGLE_PLACES",
      accessToken,
      companyName,
      `${formattedAddress}, ${companyAddressNumber}, ${city}, ${state}`
    );
    const yelpInfo = await getPlaceInfo(
      "YELP",
      accessToken,
      companyName,
      `${formattedAddress}, ${companyAddressNumber}, ${city}, ${state}`
    );
    const foursquareInfo = await getPlaceInfo(
      "FOURSQUARE",
      accessToken,
      companyName,
      `${formattedAddress}, ${companyAddressNumber}, ${city}, ${state}`
    );
    const chartData = createChartData(
      googlePlacesResults,
      yelpResults,
      foursquareResults
    );
    const tableData = createTableData(
      googlePlacesResults,
      googlePlacesInfo,
      yelpResults,
      yelpInfo,
      foursquareResults,
      foursquareInfo
    );

    const correctPercentage =
      chartData[1][1] / (chartData[1][1] + chartData[2][1] + chartData[3][1]);
    const unexistingPercentage =
      chartData[2][1] / (chartData[1][1] + chartData[2][1] + chartData[3][1]);
    const incorrectPercentage =
      chartData[3][1] / (chartData[1][1] + chartData[2][1] + chartData[3][1]);

    if (query.tipo !== "interno") {
      const lead = await saveLead(
        userName,
        userEmail,
        formatPhone(whatsapp),
        companyName,
        zipCode,
        companyAddress,
        companyAddressNumber,
        city,
        state,
        formattedPhone,
        correctPercentage,
        incorrectPercentage,
        unexistingPercentage,
        accessToken,
        query.shareable ? true : false
      );
    }

    dispatch({ type: "SET_CHART_DATA", payload: chartData });
    dispatch({ type: "SET_TABLE_DATA", payload: tableData });
    history.push("/diagnostic");
  };

  useEffect(() => {
    if (query.shareable) {
      console.log("entrou");
      onButtonClick();
    }
  }, []);

  const onCaptchaChange = (value) => {
    setState({ ...form, captcha: value });
  };

  const renderResponsive = () => {
    if (isMobile) {
      return (
        <div>
          <BlockUi tag="div" blocking={loading} loader={renderLoadingScreen}>
            <div>
              <Header />
              <Info />
              <img
                src={centralizedBackground}
                alt=""
                style={styles.mobileImageStyle}
              />
              <div style={{ marginTop: "1em" }}>
                <Form
                  handleChangeForm={handleChangeForm}
                  handleChangeCep={handleChangeCEP}
                  companyName={form.companyName}
                  errorCompanyName={errorCompanyName}
                  zipCode={form.zipCode}
                  errorZipCode={errorZipCode}
                  companyAddressNumber={form.companyAddressNumber}
                  errorCompanyAddressNumber={errorCompanyAddressNumber}
                  companyAddress={form.companyAddress}
                  errorCompanyAddress={errorCompanyAddress}
                  city={form.city}
                  errorCity={errorCity}
                  state={form.state}
                  errorState={errorState}
                  companyPhone={form.companyPhone}
                  errorCompanyPhone={errorCompanyPhone}
                  whatsapp={form.whatsapp}
                  errorWhatsapp={errorWhatsapp}
                  userName={form.userName}
                  errorUserName={errorUserName}
                  userEmail={form.userEmail}
                  errorUserEmail={errorUserEmail}
                  captcha={form.captcha}
                  onCaptchaChange={onCaptchaChange}
                  onButtonClick={onButtonClick}
                />
              </div>
            </div>
          </BlockUi>
        </div>
      );
    }
    return (
      <div>
        <BlockUi tag="div" blocking={loading} loader={renderLoadingScreen}>
          <img src={background} alt="" style={styles.backgroundBackStyle} />
          <img
            src={backgroundFront}
            alt=""
            style={styles.backgroundFrontStyle}
          />
          <div style={styles.desktopDivStyle}>
            <Header />
            <Info />
            <div style={{ marginTop: "1em" }}>
              <Form
                handleChangeForm={handleChangeForm}
                handleChangeCep={handleChangeCEP}
                companyName={form.companyName}
                errorCompanyName={errorCompanyName}
                zipCode={form.zipCode}
                errorZipCode={errorZipCode}
                companyAddressNumber={form.companyAddressNumber}
                errorCompanyAddressNumber={errorCompanyAddressNumber}
                companyAddress={form.companyAddress}
                errorCompanyAddress={errorCompanyAddress}
                city={form.city}
                errorCity={errorCity}
                state={form.state}
                errorState={errorState}
                companyPhone={form.companyPhone}
                errorCompanyPhone={errorCompanyPhone}
                whatsapp={form.whatsapp}
                errorWhatsapp={errorWhatsapp}
                userName={form.userName}
                errorUserName={errorUserName}
                userEmail={form.userEmail}
                errorUserEmail={errorUserEmail}
                captcha={form.captcha}
                onCaptchaChange={onCaptchaChange}
                onButtonClick={onButtonClick}
              />
            </div>
          </div>
        </BlockUi>
      </div>
    );
  };

  return renderResponsive();
};

export default FormPage;
