const styles = {
  imageStyle: { marginLeft: "50%", height: "100vh", position: "absolute" },
  backgroundFrontStyle: {
    //marginLeft: "45%",
    right: 0,
    height: "100vh",
    position: "absolute",
  },
  backgroundBackStyle: {
    position: "fixed",
    height: "100vh",
    width: "100vw",
    top: 0,
  },
  mobileImageStyle: { width: "96vw" },
  desktopDivStyle: { position: "absolute", margin: "2em" },
  loadingColor: { color: "#FF4040" },
  loadingTitleStyle: {
    fontSize: "70px",
    letterSpacing: "-0.1em",
    color: "#00A6FF",
  },
  loadingTextStyle: {
    fontSize: "20px",
    color: "#00A6FF",
    marginBottom: "0.5em",
  },
  loadingButtonStyle: {
    backgroundColor: "#00A6FF",
    marginTop: "0.5em",
  },
  loadingButtonTextStyle: {
    fontSize: "15px",
    color: "#FFFFFF",
  },
};

export default styles;
