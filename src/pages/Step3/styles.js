const styles = {
  textGridStyle: {
    padding: "0.5em",
    backgroundColor: "#DBDBDB",
    marginLeft: "0",
    width: "fit-content",
  },
  contentGridStyle: {
    marginLeft: "0.1%",
    padding: "0.5em",
  },
};

export default styles;
