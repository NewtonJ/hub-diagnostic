import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Button, Grid, Typography, Box, Paper } from "@material-ui/core";

import { ProgressButton } from "../../components";
import CompanyForm from "./UI/CompanyForm";
import styles from "./styles";

const Step3 = () => {
  const handleButtonClick = () => {};
  const { numberOfAddresses, companyForms } = useSelector(
    (state) => state.CompanyInfoReducer
  );
  const [form, setState] = useState(companyForms);

  const handleChangeForm = ({ target: { name, value } }, index) => {
    const formCopy = form;
    formCopy[index][name] = value;
    setState(formCopy);
  };

  return (
    <Grid container direction="column" alignItems="flex-start">
      <Grid
        container
        direction="row"
        justify="flex-start"
        style={styles.textGridStyle}
      >
        <Typography>
          <Box fontWeight="fontWeightBold" fontFamily="Poppins" m={0.5}>
            [3/4] Terceiro Passo:
          </Box>
        </Typography>
        <Typography>
          <Box fontWeight="fontWeightRegular" fontFamily="Poppins" m={0.5}>
            informe o(s) endereço(s) da sua empresa
          </Box>
        </Typography>
      </Grid>
      <Paper elevation={0} style={{ background: "#4287f5" }}>
        <Grid
          container
          spacing={3}
          justify="space-around"
          alignItems="center"
          style={styles.contentGridStyle}
        >
          <Grid item>
            <Grid container spacing={3}>
              <Grid item>
                <Button style={{ backgroundColor: "#FFFFFF" }}>
                  Importar em Massa
                </Button>
              </Grid>
              <Grid item>
                <Button style={{ backgroundColor: "#FFFFFF" }}>
                  Baixar Planilha de importação
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <ProgressButton onClick={handleButtonClick} />
          </Grid>
        </Grid>
        <Grid container justify="center">
          {form.map((element, index) => (
            <Grid item>
              <CompanyForm value={form[index]} />
            </Grid>
          ))}
        </Grid>
      </Paper>
    </Grid>
  );
};

export default Step3;
