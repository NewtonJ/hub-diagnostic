import React from "react";
import { TextField } from "@material-ui/core";

export default ({ placeholder, width, name, value, onChangeText }) => (
  <TextField
    variant="outlined"
    placeholder={placeholder}
    name={name}
    value={value}
    onChange={onChangeText}
    style={{ backgroundColor: "#FFFFFF", width: width }}
  />
);
