import React from "react";
import { Grid, TextField } from "@material-ui/core";

import FormInput from "./FormInput";
import styles from "./styles";

export default ({ value, onChangeText }) => (
  <Grid container direction="row" style={styles.companyGridStyle} spacing={2}>
    <Grid item>
      <FormInput
        placeholder="Nome da Empresa"
        width="7em"
        value={value.companyName}
        name="companyName"
      />
    </Grid>
    <Grid item>
      <FormInput placeholder="CEP" width="7em" value={value.CEP} name="CEP" />
    </Grid>
    <Grid item>
      <FormInput
        placeholder="Bairro"
        width="7em"
        value={value.neighborhood}
        name="neighborhood"
      />
    </Grid>
    <Grid item>
      <FormInput
        placeholder="Cidade"
        width="7em"
        value={value.city}
        name="city"
      />
    </Grid>
    <Grid item>
      <FormInput
        placeholder="País"
        width="7em"
        value={value.country}
        name="country"
      />
    </Grid>
    <Grid item>
      <FormInput
        placeholder="Endereço e Número"
        width="7em"
        value={value.address}
        name="address"
      />
    </Grid>
    <Grid item>
      <FormInput
        placeholder="Horário que abre"
        width="7em"
        value={value.openingHour}
        name="openingHour"
      />
    </Grid>
    <Grid item>
      <FormInput
        placeholder="Horário que fecha"
        width="7em"
        value={value.closingHour}
        name="closingHour"
      />
    </Grid>
    <Grid item>
      <FormInput
        placeholder="Telefone principal para contato"
        width="7em"
        value={value.phone}
        name="phone"
      />
    </Grid>
  </Grid>
);
