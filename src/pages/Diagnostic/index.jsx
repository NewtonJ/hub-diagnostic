import React, { useRef } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Chart } from "react-google-charts";
import { Paper, Divider, Grid } from "@material-ui/core";
import { PDFExport } from "@progress/kendo-react-pdf";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import { isMobile } from "react-device-detect";
import { Typography } from "@material-ui/core";
import copy from "clipboard-copy";

import Header from "./UI/Header";
import PaperTitle from "./UI/PaperTitle";
import Status from "./UI/Status";
import SearchResults from "./UI/SearchResults";

const Diagnostic = () => {
  const history = useHistory();
  const myRef = useRef(null);
  const { chartData, tableData } = useSelector(
    (state) => state.DiagnosticReducer
  );

  const searchData = useSelector(
    (state) => state.CompanyInfoReducer.companyData
  );

  const companyExistsInGoogle = useSelector(
    (state) => state.CompanyInfoReducer.companyExistsInGoogle
  );
  const companyExistsInYelp = useSelector(
    (state) => state.CompanyInfoReducer.companyExistsInYelp
  );
  const companyExistsInFoursquare = useSelector(
    (state) => state.CompanyInfoReducer.companyExistsInFoursquare
  );

  return (
    <div>
      <Header text="Seu relatório está pronto!" />
      {/* <PDFExport
        ref={myRef}
        paperSize="auto"
        margin={40}
        fileName="diagnostic"
        author="HubLocal"
      > */}
      <Paper id="diagnostic">
        <PaperTitle
          title={`Relatório de Presença Online - ${searchData.companyName}`}
          onShareClicked={() => {
            copy(
              `${window.location.origin.toString()}/?companyName=${
                searchData.companyName
              }&companyAddress=${
                searchData.companyAddress
              }&companyAddressNumber=${searchData.companyAddressNumber}&city=${
                searchData.city
              }&state=${searchData.state}&companyPhone=${
                searchData.companyPhone
              }&zipCode=${searchData.zipCode}&whatsapp=${
                searchData.whatsapp
              }&userName=${searchData.userName}&userEmail=${
                searchData.userEmail
              }&shareable=true`.replace(/ /g, "%20")
            );
            // history.push(
            //   `/?companyName=${searchData.companyName}&companyAddress=${searchData.companyAddress}&companyAddressNumber=${searchData.companyAddressNumber}&city=${searchData.city}&state=${searchData.state}&companyPhone=${searchData.companyPhone}&zipCode=${searchData.zipCode}&whatsapp=${searchData.whatsapp}&userName=${searchData.userName}&userEmail=${searchData.userEmail}&shareable=true`
            // );
          }}
          onExportPDFClicked={() => {
            // const input = document.getElementById("diagnostic");
            // html2canvas(input).then((canvas) => {
            //   canvas.width(500);
            //   const imgData = canvas.toDataURL("image/png");
            //   const pdf = new jsPDF({ orientation: "p", format: "a4" });
            //   pdf.addImage(imgData, "PNG", 0, 0);
            //   pdf.save("diagnostico.pdf");
            // });
            // myRef.current.save();
            window.print();
          }}
        />
        <Divider />
        <Grid container spacing={2} justify="center" alignItems="center">
          <Grid item>
            <Chart
              width={isMobile ? "30em" : "40em"}
              height={isMobile ? "20em" : "30em"}
              chartType="PieChart"
              loader={<div>Loading Chart</div>}
              data={chartData}
              options={{
                title:
                  "Qualidade dos seus dados de localização online no principais mapas e listas da internet",
                colors: ["#5BC835", "#FF4E5A", "#FBBC04"],
                titleTextStyle: { fontName: "Poppins" },
              }}
            />
          </Grid>
          <Grid item>
            <Status
              status={chartData}
              onOptimizeClicked={() => {
                window.open(
                  "https://hublocal.pipedrive.com/scheduler/92ar0XCR/hublocal-ativacao-trial-30-dias"
                );
              }}
              onNewDiagnosticClicked={() => {
                history.push("/");
              }}
            />
          </Grid>
        </Grid>
        <SearchResults
          data={tableData}
          searchData={searchData}
          companyExistsInGoogle={companyExistsInGoogle}
          companyExistsInFoursquare={companyExistsInFoursquare}
          companyExistsInYelp={companyExistsInYelp}
        />
      </Paper>
      {/* </PDFExport> */}
    </div>
  );
};

export default Diagnostic;
