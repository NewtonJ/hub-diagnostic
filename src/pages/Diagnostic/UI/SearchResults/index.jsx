import React, { useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Typography,
  Tooltip,
  Link,
} from "@material-ui/core";
import { isMobile } from "react-device-detect";

import facebook from "../../../../assets/images/Facebook Places.png";
import linkedin from "../../../../assets/images/linkedin.png";
import whatsapp from "../../../../assets/images/Whatsapp1080.png";
import styles from "./styles";

import { getStatusIcon, getWeekday, checkEngine } from "./utils";

const SearchResult = ({
  data,
  searchData,
  companyExistsInGoogle,
  companyExistsInYelp,
  companyExistsInFoursquare,
}) => {
  const checkStatus = (status, tooltipText) => {
    return (
      <div
        onClick={() => {
          if (isMobile) alert(tooltipText);
        }}
      >
        <Tooltip
          title={
            <Typography style={styles.tooltipTextStyle}>
              {tooltipText}
            </Typography>
          }
        >
          {getStatusIcon(status)}
        </Tooltip>
      </div>
    );
  };

  const renderCompanyNotFound = (element) => {
    if (element.engine === "GOOGLE_MAPS" && companyExistsInGoogle.found) {
      return (
        <Tooltip
          title={
            <Typography style={styles.tooltipTextStyle}>
              {companyExistsInGoogle.result.formatted_address}
            </Typography>
          }
        >
          <Typography
            style={styles.companyCheckTextStyle}
            onClick={() => {
              if (isMobile)
                alert(companyExistsInGoogle.result.formatted_address);
            }}
          >
            *Uma empresa com o mesmo nome foi encontrada em outro endereço
          </Typography>
        </Tooltip>
      );
    }
    if (element.engine === "WAZE" && companyExistsInGoogle.found) {
      return (
        <Tooltip
          title={
            <Typography style={styles.tooltipTextStyle}>
              {companyExistsInGoogle.result.formatted_address}
            </Typography>
          }
        >
          <Typography
            style={styles.companyCheckTextStyle}
            onClick={() => {
              if (isMobile)
                alert(companyExistsInGoogle.result.formatted_address);
            }}
          >
            *Uma empresa com o mesmo nome foi encontrada em outro endereço
          </Typography>
        </Tooltip>
      );
    }
    if (element.engine === "YELP" && companyExistsInYelp.found) {
      return (
        <Tooltip
          title={
            <Typography style={styles.tooltipTextStyle}>
              {`${companyExistsInYelp.result.location.address1}, ${companyExistsInYelp.result.location.address2}`}
            </Typography>
          }
        >
          <Typography
            style={styles.companyCheckTextStyle}
            onClick={() => {
              if (isMobile)
                alert(
                  `${companyExistsInYelp.result.location.address1}, ${companyExistsInYelp.result.location.address2}`
                );
            }}
          >
            *Uma empresa com o mesmo nome foi encontrada em outro endereço
          </Typography>
        </Tooltip>
      );
    }
    if (element.engine === "FOURSQUARE" && companyExistsInFoursquare.found) {
      return (
        <Tooltip
          title={
            <Typography style={styles.tooltipTextStyle}>
              {companyExistsInFoursquare.result.location.address}
            </Typography>
          }
        >
          <Typography
            style={styles.companyCheckTextStyle}
            onClick={() => {
              if (isMobile)
                alert(companyExistsInFoursquare.result.location.address);
            }}
          >
            *Uma empresa com o mesmo nome foi encontrada em outro endereço
          </Typography>
        </Tooltip>
      );
    }
    if (element.engine === "APPLE_MAPS" && companyExistsInFoursquare.found) {
      return (
        <Tooltip
          title={
            <Typography style={styles.tooltipTextStyle}>
              {companyExistsInFoursquare.result.location.address}
            </Typography>
          }
        >
          <Typography
            style={styles.companyCheckTextStyle}
            onClick={() => {
              if (isMobile)
                alert(companyExistsInFoursquare.result.location.address);
            }}
          >
            *Uma empresa com o mesmo nome foi encontrada em outro endereço
          </Typography>
        </Tooltip>
      );
    }
  };

  const renderCompanyNameWhenNotFound = (element) => {
    if (element.engine === "GOOGLE_MAPS") {
      if (companyExistsInGoogle.found) {
        return companyExistsInGoogle.result.name;
      }
    }
    if (element.engine === "YELP") {
      if (companyExistsInYelp.found) {
        return companyExistsInYelp.result.name;
      }
    }
    if (element.engine === "FOURSQUARE") {
      if (companyExistsInFoursquare.found) {
        return companyExistsInFoursquare.result.name;
      }
    }
    if (element.engine === "WAZE") {
      if (companyExistsInGoogle.found) {
        return companyExistsInGoogle.result.name;
      }
    }
    if (element.engine === "APPLE_MAPS") {
      if (companyExistsInFoursquare.found) {
        return companyExistsInFoursquare.result.name;
      }
    }
    return `Não encontrado`;
  };

  const renderCompanyText = (element) => {
    if (element.notFound) {
      return (
        <div>
          <Typography style={styles.notFoundStyle}>
            {renderCompanyNameWhenNotFound(element)}
          </Typography>
          {renderCompanyNotFound(element)}
        </div>
      );
    }
    return (
      <Typography style={styles.addressTextStyle}>
        {element.companyAddress.street}
        {element.companyAddress.street ? "," : ""}{" "}
        {element.companyAddress.number}
        {element.companyAddress.number ? "," : ""}{" "}
        {element.companyAddress.neighborhod}
        {element.companyAddress.neighborhod ? "," : ""}{" "}
        {element.companyAddress.city}
        {element.companyAddress.city ? "," : ""} {element.companyAddress.state}
        {element.companyAddress.state ? "," : ""}{" "}
        {element.companyAddress.zip_code}
      </Typography>
    );
  };

  const renderWorkingHours = (workingHours) => {
    if (isMobile) {
      let workingHoursString = ``;
      if (Array.isArray(workingHours)) {
        workingHours.map((element) => {
          workingHoursString =
            workingHoursString +
            `${getWeekday(element.week_day)}: ${element.opening_hour} - ${
              element.closing_hour
            }\n`;
        });
      }
      return workingHoursString;
    }
    if (Array.isArray(workingHours)) {
      return (
        <div>
          {workingHours.map((element) => (
            <Typography style={styles.tooltipTextStyle}>{`${getWeekday(
              element.week_day
            )}: ${element.opening_hour} - ${element.closing_hour}`}</Typography>
          ))}
        </div>
      );
    }
  };

  const renderPhoto = (photo) => {
    if (photo !== "not found") {
      return (
        <>
          <Typography style={styles.tooltipTextStyle}>
            Clique para abrir a foto
          </Typography>
          <img src={photo} style={{ width: "10em", height: "10em" }} />
        </>
      );
    }
  };

  const renderCompanyName = (element) => {
    return (
      <Link
        component="button"
        variant="body2"
        onClick={() => {
          window.open(element.url);
        }}
      >
        <Typography
          style={
            element.nameStatus == "Correto" &&
            element.phoneStatus === "Correto" &&
            element.openingHoursStatus === "Existente" &&
            element.photoStatus === "Existente"
              ? styles.allCorrectCompanyNameStyle
              : styles.companyNameStyle
          }
        >
          {element.companyName}
        </Typography>
      </Link>
    );
  };

  return (
    <div>
      <div style={styles.titleDivStyle}>
        <Typography style={styles.titleStyle}>Resultados</Typography>
        <Typography style={styles.subTitleStyle}>
          A visibilidade da sua empresa online
        </Typography>
      </div>
      <TableContainer>
        <Table>
          <TableHead style={styles.tableHeaderStyle}>
            <TableRow>
              <TableCell style={styles.tableHeaderTextStyle}>
                Diretório
              </TableCell>
              <TableCell style={styles.tableHeaderTextStyle}>Empresa</TableCell>
              <TableCell style={styles.tableHeaderTextStyle}>Nome</TableCell>
              <TableCell style={styles.tableHeaderTextStyle}>
                Endereço
              </TableCell>
              <TableCell style={styles.tableHeaderTextStyle}>
                Telefone
              </TableCell>
              <TableCell style={styles.tableHeaderTextStyle}>Horário</TableCell>
              <TableCell style={styles.tableHeaderTextStyle}>Fotos</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((element) => {
              return (
                <TableRow>
                  <TableCell>{checkEngine(element.engine)}</TableCell>
                  <TableCell>
                    {/* <Typography style={styles.companyNameStyle}>
                      {element.companyName}
                    </Typography> */}
                    {renderCompanyName(element)}
                    {renderCompanyText(element)}
                  </TableCell>
                  <TableCell>
                    {checkStatus(
                      element.nameStatus,
                      `Nome buscado: ${searchData.companyName}`
                    )}
                  </TableCell>
                  <TableCell>
                    {checkStatus(
                      element.addressStatus,
                      `Endereço buscado: ${searchData.companyAddress}, ${searchData.companyAddressNumber}, ${searchData.city}, ${searchData.state}, ${searchData.zipCode}`
                    )}
                  </TableCell>
                  <TableCell>
                    {checkStatus(
                      element.phoneStatus,
                      `Telefone buscado: ${searchData.companyPhone}\n Telefone encontrado: ${element.phone}`
                    )}
                  </TableCell>
                  <TableCell>
                    {checkStatus(
                      element.openingHoursStatus,
                      element.error
                        ? ""
                        : renderWorkingHours(element.openingHours)
                    )}
                  </TableCell>
                  <TableCell
                    onClick={() => {
                      window.open(element.photo);
                    }}
                  >
                    {/* {checkStatus(element.photoStatus, "Clique para abrir a foto")} */}
                    {checkStatus(
                      element.photoStatus,
                      renderPhoto(element.photo)
                    )}
                  </TableCell>
                </TableRow>
              );
            })}
            <TableRow>
              <TableCell>
                <div style={styles.engineDivStyle}>
                  <img src={facebook} style={styles.iconStyle} />
                  <Typography style={styles.engineTextStyle}>
                    Facebook
                  </Typography>
                </div>
              </TableCell>
              <TableCell>
                <Typography style={styles.workInProgressStyle}>
                  Indisponível
                </Typography>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <div style={styles.engineDivStyle}>
                  <img src={whatsapp} style={styles.iconStyle} />
                  <Typography style={styles.engineTextStyle}>
                    Whatsapp
                  </Typography>
                </div>
              </TableCell>
              <TableCell>
                <Typography style={styles.workInProgressStyle}>
                  Indisponível
                </Typography>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <div style={styles.engineDivStyle}>
                  <img src={linkedin} style={styles.iconStyle} />
                  <Typography style={styles.engineTextStyle}>
                    Linkedin
                  </Typography>
                </div>
              </TableCell>
              <TableCell>
                <Typography style={styles.workInProgressStyle}>
                  Indisponível
                </Typography>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default SearchResult;
