const styles = {
  iconStyle: { height: "2em", width: "2em" },
  correctIconStyle: { color: "#0F9D58" },
  incorrectIconStyle: { color: "#FBBC04" },
  unexistingIconStyle: { color: "#EA4335" },
  titleDivStyle: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  titleStyle: { fontFamily: "Poppins", fontSize: "25px" },
  subTitleStyle: { fontFamily: "Poppins", marginBottom: "1em" },
  tableHeaderStyle: { backgroundColor: "#4285F4" },
  tableHeaderTextStyle: { color: "#FFFFFF", fontFamily: "Poppins" },
  engineTextStyle: {
    fontFamily: "Poppins",
    color: "#4285F4",
    fontWeight: "bold",
  },
  engineDivStyle: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
  },
  notFoundStyle: {
    fontFamily: "Poppins",
    color: "#EA4335",
    fontWeight: "bold",
    fontSize: "16px",
  },
  workInProgressStyle: {
    fontFamily: "Poppins",
    color: "#EA4335",
    fontWeight: "bold",
    fontSize: "16px",
  },
  allCorrectCompanyNameStyle: {
    color: "#0F9D58",
    fontFamily: "Poppins",
    fontWeight: "bold",
  },
  companyNameStyle: {
    fontFamily: "Poppins",
    fontWeight: "bold",
    color: "#F8AC00",
  },
  addressTextStyle: {
    fontFamily: "Poppins",
    fontSize: "13px",
    fontWeight: "bold",
  },
  tooltipTextStyle: {
    fontFamily: "Poppins",
  },
  companyCheckTextStyle: {
    fontSize: "0.8em",
    fontFamily: "Poppins",
    fontWeight: "1000",
  },
};

export default styles;
