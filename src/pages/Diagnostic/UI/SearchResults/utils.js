import React from "react";
import { Typography } from "@material-ui/core";

import correct from "../../../../assets/images/Correto.png";
import incorrect from "../../../../assets/images/Incorreto.png";
import unexisting from "../../../../assets/images/Inexistente.png";
import googleMaps from "../../../../assets/images/Google Maps.png";
import yelp from "../../../../assets/images/Yelp1080.png";
import foursquare from "../../../../assets/images/Foursquare.png";
import waze from "../../../../assets/images/Waze1080.png";
import appleMaps from "../../../../assets/images/AppleMaps.png";
import styles from "./styles";

export const getWeekday = (weekday) => {
  switch (weekday) {
    case "Monday":
      return "Segunda-feira";
    case "Tuesday":
      return "Terça-feira";
    case "Wednesday":
      return "Quarta-feira";
    case "Thursday":
      return "Quinta-feira";
    case "Friday":
      return "Sexta-feira";
    case "Saturday":
      return "Sábado";
    case "Sunday":
      return "Domingo";
    default:
      return "";
  }
};

export const getStatusIcon = (status) => {
  let image;
  switch (status) {
    case "Correto":
      image = correct;
      break;
    case "Incorreto":
      image = incorrect;
      break;
    case "Inexistente":
      image = unexisting;
      break;
    case "Existente":
      image = correct;
      break;
    case "not found":
      image = unexisting;
      break;
    default:
      image = unexisting;
      break;
  }
  return <img src={image} style={styles.iconStyle} />;
};

export const checkEngine = (engine) => {
  let image, name;
  switch (engine) {
    case "GOOGLE_MAPS":
      image = googleMaps;
      name = "Google Maps";
      break;
    case "WAZE":
      image = waze;
      name = "Waze";
      break;
    case "YELP":
      image = yelp;
      name = "Yelp";
      break;
    case "FOURSQUARE":
      image = foursquare;
      name = "Foursquare";
      break;
    case "APPLE_MAPS":
      image = appleMaps;
      name = "Apple Maps";
    default:
      break;
  }
  return (
    <div style={styles.engineDivStyle}>
      <img src={image} style={styles.iconStyle} />
      <Typography style={styles.engineTextStyle}>{name}</Typography>
    </div>
  );
};
