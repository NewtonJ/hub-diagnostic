const styles = {
  headerStyle: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: "2em",
  },
  paperTitleStyle: { fontFamily: "Poppins", fontSize: "25px" },
  mobilePaperTitleStyle: { fontFamily: "Poppins", fontSize: "1.2em" },
  paperHeaderStyle: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: "1em",
  },
  titleDisplayStyle: {
    display: "contents",
  },
  fontStyle: { fontSize: "30px", fontFamily: "Poppins", marginTop: "0.5em" },
  mobileFontStyle: { fontSize: "1em", fontFamily: "Poppins" },
  logoStyle: { height: "4em", width: "12em", marginTop: "0.5em" },
  mobileLogoStyle: { height: "3em", width: "10em" },
  statusTextStyle: {
    fontSize: "20px",
    fontFamily: "Poppins",
    fontWeight: "bold",
    marginBottom: "1em",
  },
  statusOptionsTextStyle: { fontFamily: "Poppins" },
  statusOptionsGreenTextStyle: {
    fontFamily: "Poppins",
    fontWeight: "bold",
    color: "#0F9D58",
  },
  statusOptionsYellowTextStyle: {
    fontFamily: "Poppins",
    fontWeight: "bold",
    color: "#FBBC04",
  },
  statusOptionsRedTextStyle: {
    fontFamily: "Poppins",
    fontWeight: "bold",
    color: "#EA4335",
  },
  buttonStyle: {
    backgroundColor: "#FBBC04",
    marginTop: "2em",
    marginBottom: "2em",
    borderRadius: "4m",
  },
  relevanceImageStyle: { width: "15em", height: "8em" },
  buttonSelectedStyle: {
    backgroundColor: "#34A853",
    marginTop: "2em",
    marginBottom: "2em",
  },
  buttonTextStyle: { color: "#FFFFFF" },
  lowRelevancyTextStyle: {
    color: "#EA4335",
    fontSize: "25px",
    marginBottom: "1em",
  },
  goodRelevancyTextStyle: {
    color: "#0F9D58",
    fontSize: "25px",
    marginBottom: "1em",
  },
  iconStyle: { height: "1.5em", width: "1.5em" },
};

export default styles;
