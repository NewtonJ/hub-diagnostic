import React, { useState } from "react";
import { Typography, Button, Grid, ButtonBase } from "@material-ui/core";

import check from "../../../assets/images/Check.png";
import HighRelevance from "../../../assets/images/HighRelevance.png";
import LowRelevance from "../../../assets/images/LowRelevance.png";
import TestButton from "../../../assets/images/30dayTestButton.png";
import styles from "./styles";

const Status = ({ status, onOptimizeClicked, onNewDiagnosticClicked }) => {
  const correctPercentage =
    status[1][1] / (status[1][1] + status[2][1] + status[3][1]);

  const renderRelevancyText = () => {
    if (correctPercentage < 0.5) {
      return <img src={LowRelevance} style={styles.relevanceImageStyle} />;
    }
    return <img src={HighRelevance} style={styles.relevanceImageStyle} />;
  };

  return (
    <div>
      {renderRelevancyText()}
      <Typography style={styles.statusTextStyle}>
        Otimize a presença online da sua empresa
      </Typography>
      <div style={{ marginLeft: "2em" }}>
        <Grid container spacing={1} alignItems="center">
          <Grid item>
            <img src={check} style={styles.iconStyle} />
          </Grid>
          <Grid item>
            <Typography style={styles.statusOptionsTextStyle}>
              Corrija tudo oque estiver
            </Typography>
          </Grid>
          <Grid item>
            <Typography style={styles.statusOptionsYellowTextStyle}>
              incorreto
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={1} alignItems="center">
          <Grid item>
            <img src={check} style={styles.iconStyle} />
          </Grid>
          <Grid item>
            <Typography style={styles.statusOptionsTextStyle}>
              Cadastre sua empresa onde estiver
            </Typography>
          </Grid>
          <Grid item>
            <Typography style={styles.statusOptionsRedTextStyle}>
              {" "}
              inexistente
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={1} alignItems="center">
          <Grid item>
            <img src={check} style={styles.iconStyle} />
          </Grid>
          <Grid item>
            <Typography style={styles.statusOptionsTextStyle}>
              Mantenha tudo atualizado e
            </Typography>
          </Grid>
          <Grid item>
            <Typography style={styles.statusOptionsGreenTextStyle}>
              {" "}
              correto
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={1} alignItems="center">
          <Grid item>
            <img src={check} style={styles.iconStyle} />
          </Grid>
          <Grid item>
            <Typography style={styles.statusOptionsTextStyle}>
              Transforme sua empresa numa
            </Typography>
          </Grid>
          <Grid item>
            <Typography style={styles.statusOptionsGreenTextStyle}>
              máquina de atrair clientes
            </Typography>
          </Grid>
        </Grid>
      </div>
      <Grid container spacing={1} alignItems="center">
        <Grid item>
          <ButtonBase onClick={onOptimizeClicked}>
            <img src={TestButton} style={{ width: "18em" }} />
          </ButtonBase>
        </Grid>
        <Grid item>
          <Button style={styles.buttonStyle} onClick={onNewDiagnosticClicked}>
            <Typography style={styles.buttonTextStyle}>
              Fazer novo diagnóstico
            </Typography>
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

export default Status;
