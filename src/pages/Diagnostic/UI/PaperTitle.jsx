import React, { useState } from "react";
import {
  Typography,
  Grid,
  Link,
  Tooltip,
  ClickAwayListener,
} from "@material-ui/core";

import print from "../../../assets/images/print.png";
import share from "../../../assets/images/share.png";
import styles from "./styles";

const isMobile = window.innerWidth <= 500 || window.innerHeight <= 500;
export default ({ title, onExportPDFClicked, onShareClicked }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);

  return (
    <div style={styles.paperHeaderStyle}>
      <Typography
        style={isMobile ? styles.mobilePaperTitleStyle : styles.paperTitleStyle}
      >
        {title}
      </Typography>
      <Grid container spacing={2} justify="center" alignItems="center">
        <Grid item>
          <Grid container alignItem="center">
            <Grid item>
              <img
                src={share}
                style={{ width: "1em", height: "1em", marginRight: "0.2em" }}
              />
            </Grid>
            <Grid item>
              <ClickAwayListener
                onClickAway={() => {
                  setTooltipOpen(false);
                }}
              >
                <Tooltip
                  PopperProps={{
                    disablePortal: true,
                  }}
                  open={tooltipOpen}
                  onClose={() => {
                    setTooltipOpen(false);
                  }}
                  disableFocusListener
                  disableHoverListener
                  disableTouchListener
                  title="Link copiado para a área de transferência"
                >
                  <Link
                    component="button"
                    variant="body2"
                    onClick={() => {
                      setTooltipOpen(true);
                      onShareClicked();
                    }}
                  >
                    Compartilhar link do relatório
                  </Link>
                </Tooltip>
              </ClickAwayListener>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container alignItem="center">
            <Grid item>
              <img
                src={print}
                style={{ width: "1em", height: "1em", marginRight: "0.2em" }}
              />
            </Grid>
            <Grid item>
              <Link
                component="button"
                variant="body2"
                onClick={onExportPDFClicked}
              >
                Imprimir
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};
